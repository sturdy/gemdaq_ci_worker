# Custom worker image for Jenkins and GitLab-CI at CERN
[![build status](https://gitlab.cern.ch/cms-gem-daq-project/gemdaq_ci_worker/badges/master/coverage.svg)](https://gitlab.cern.ch/cms-gem-daq-project/gemdaq_ci_worker/commits/master)
[![coverage](https://gitlab.cern.ch/cms-gem-daq-project/gemdaq_ci_worker/badges/master/build.svg)](https://gitlab.cern.ch/cms-gem-daq-project/gemdaq_ci_worker/commits/master)

This project is copied from a sample of a customized slave image for Jenkins.
See http://cern.ch/jenkinsdocs/chapters/slaves/custom-docker.html for more information.

This project builds two docker images, based on the both CC7 and SLC6 slaves.
To push two or more images to the same repository on the [GitLab Registry](https://cern.service-now.com/service-portal/article.do?n=KB0004284), just use different `tags` for each one.

After the images are built, both of them are used to run a job inside GitLab-CI. This demonstrates the compatibility of the images with GitLab-CI and prints out information about the installed packages.

Check the `.gitlab-ci.yml` of this repository for more information.

## How to use

* Fork this project
* Edit the Dockerfile (look for TODO lines) and the GitLab-CI build to install the packages required for your Jenkins jobs
* Follow [the instructions in the CERN Jenkins documentation](http://cern.ch/jenkinsdocs/chapters/slaves/custom-docker.html)
to add this custom image to your Jenkins instance if necessary.

## Structure of the Docker containers
The base containers are derived from the CERN `slc6` and `cc7` images, with the appropriate `xdaq` packages installed.
Since the release of `xdaq 14.6` for `cc7`, an additional image is provided to check builds against the `GitLab` version of the codebase, compared with the `svn` version.

### Containers

* `base:slc6`
* `devtools:slc6`
* `extrapy:slc6`
* `withroot:slc6`
* `extrapy/witroot:slc6`
* `extrapy/devtools:slc6`
* `devtools/witroot:slc6`
* `base:cc7`
* `devtools:cc7`
* `extrapy:cc7`
* `withroot:cc7`
* `extrapy/witroot:cc7`
* `extrapy/devtools:cc7`
* `devtools/witroot:cc7`

### Provided packages
* All containers are set up the necessary `xdaq` dependencies
  * `slc6` is only run with the `svn` version of the codebase, and `xdaq 13.X.Y`
  * `cc7` is run with the `svn` version of the codebase (`xdaq <14.6`), as well as the `GitLab` version (`xdaq 14.6.Y+`)
* `rh-git29` is provided and can be enabled with:
  * `scl enable rh-git29` or
  * `source /opt/rh/git29/enable`
* Several `python` versions are installed (in the images `extrapy:slc6` and `extrapy:cc7`)
  * `python`: `2.7, 3.3, 3.4, 3.5, 3.5`
    * `python27`, `python33`, and `python34` are installed and can be enabled with:
      * `scl enable python3X` or
      * `source /opt/rh/python3X/enable`
    * `rh-python34`, `rh-python-35`, and `rh-python-36` are installed and can be enabled with:
      * `scl enable rh-python3X` or
      * `source /opt/rh/rh-python3X/enable`
  * `pypy`: system default
* Several `ROOT` versions are installed (in the images `withroot:slc6` and `withroot:cc7`)
  * `ROOT`: `5.34.28` and `5.34.36` for both `slc6` and `cc7`, and `6.10.08` and `6.12.06` for `cc7` only
    * In the `slc6` image, these are compiled for `gcc` versions: `4.4`, `4.7`, `4.8`, `4.9`, and `5.1`
    * In the `cc7` image, these are compiled for `gcc` version: `4.8`
* Several `devtoolset` versions are installed (in the images `devtools:slc6` and `devtools:cc7`)
  * `devtoolset`: `3`
    * `gcc`: `4.9.2`
    * `oprofile`: `0.9.9`
    * `valgrind`: `3.10.1`
  * `devtoolset`: `4`
    * `gcc`: `5.3.1`
    * `oprofile`: `1.1.0`
    * `valgrind`: `3.11.0`
  * `devtoolset`: `6`
    * `gcc`: `6.3.1`
    * `make`: `4.1`
    * `oprofile`: `1.1.0`
    * `valgrind`: `3.12.0`
  * `devtoolset`: `7`
    * `gcc`: `7.2.1`
    * `llvm`: `7.0`
    * `make`: `4.2.1`
    * `oprofile`: `1.2.0`
    * `valgrind`: `3.13.0`
  * Different `devtoolset` packages can be enabled with:
    * `scl enable devtoolset-X` or
    * `source /opt/rh/devtoolset-X/enable`
* `ruby`: `2.2, 2.3, 2.4` are provided (in the images `extrapy:slc6` and `extrapy:cc7`)
  * `rh-ruby22`, `rh-ruby23`, and `rh-ruby24` is provided and can be enabled with:
    * `scl enable rh-ruby2X` or
    * `source /opt/rh/ruby2X/enable`
  * `rubygems` is provided and the following `gems` installed (for `rh-ruby24`):
    * `bundler`: 
    * `ghi`: 
    * `github_changelog_generator`: 
    * `katip`: 
