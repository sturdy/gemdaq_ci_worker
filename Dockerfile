# Sample Dockerfile to build a custom Jenkins slave image to be used
# with CERN Jenkins instances (cf. http://cern.ch/jenkinsdocs)

# The FROM statement can be overriden in the GitLab-CI build
# Use tag 'cc7' instead of 'slc6' for CC7
# How do we ensure that this always pulls a non-modified container?
# The log seems to indicate that sometimes artifacts are kept
FROM gitlab-registry.cern.ch/ci-tools/ci-worker:slc6

LABEL maintainer "Jared Sturdy sturdy@cern.ch"

# The default build arch for now is slc6 and can be overridden in the GitLab-CI build
ARG BUILD_ARCH=
ARG EXTEND=
ARG DEVTOOLS=
ARG EXTRAROOT=
ARG EXTRAPYTHON=
ARG EXTRARUBY=

RUN env; \
if [ "${EXTEND}" = "0" ]; \
then \
    # Set up the yum repos for the various necessary packages
    if [ "$BUILD_ARCH" = "slc6" ]; \
    then \
        echo "Building slc6 cmsgemos Docker image"; \
        echo \
$'[xdaq-base]\n\
name     = XDAQ Software Base\n\
baseurl  = http://xdaq.web.cern.ch/xdaq/repo/13/slc6x/x86_64/base/RPMS/\n\
enabled  = 1\n\
gpgcheck = 0\n\
\n\
[xdaq-updates]\n\
name     = XDAQ Software Updates\n\
baseurl  = http://xdaq.web.cern.ch/xdaq/repo/13/slc6x/x86_64/updates/RPMS/\n\
enabled  = 1\n\
gpgcheck = 0\n\
\n\
[xdaq-extras]\n\
name     = XDAQ Software Extras\n\
baseurl  = http://xdaq.web.cern.ch/xdaq/repo/13/slc6x/x86_64/extras/RPMS/\n\
enabled  = 1\n\
gpgcheck = 0\n\
\n\
[xdaq-kernel-modules]\n\
name     = XDAQ Kernel Modules\n\
baseurl  = http://xdaq.web.cern.ch/xdaq/repo/13/slc6x/x86_64/kernel_modules/RPMS/\n\
enabled  = 1\n\
gpgcheck = 0\n'\
        >/etc/yum.repos.d/xdaq.repo; \

        echo \
$'[ipbus-sw-uhal24-base]\n\
name     = IPBus Software Repository (version 2.4)\n\
baseurl  = http://www.cern.ch/cactus/release/2.4/slc6_x86_64/base/RPMS\n\
enabled  = 0\n\
gpgcheck = 0\n\
\n\
[ipbus-sw-uhal24-updates]\n\
name     = IPBus Software Repository (version 2.4) updates\n\
baseurl  = http://www.cern.ch/cactus/release/2.4/slc6_x86_64/updates/RPMS\n\
enabled  = 0\n\
gpgcheck = 0\n\
\n\
[ipbus-sw-uhal25-base]\n\
name     = IPBus Software Repository (version 2.5)\n\
baseurl  = http://www.cern.ch/ipbus/sw/release/2.5/slc6_x86_64/base/RPMS\n\
enabled  = 0\n\
gpgcheck = 0\n\
\n\
[ipbus-sw-uhal25-updates]\n\
name     = IPBus Software Repository (version 2.5) updates\n\
baseurl  = http://www.cern.ch/ipbus/sw/release/2.5/slc6_x86_64/updates/RPMS\n\
enabled  = 0\n\
gpgcheck = 0\n\
\n\
[ipbus-sw-uhal26-base]\n\
name     = IPBus Software Repository (version 2.6)\n\
baseurl  = http://www.cern.ch/ipbus/sw/release/2.6/repos/slc6_x86_64/base/RPMS\n\
enabled  = 1\n\
gpgcheck = 0\n\
\n\
[ipbus-sw-uhal26-updates]\n\
name     = IPBus Software Repository (version 2.6) updates\n\
baseurl  = http://www.cern.ch/ipbus/sw/release/2.6/repos/slc6_x86_64/updates/RPMS\n\
enabled  = 1\n\
gpgcheck = 0\n'\
        >/etc/yum.repos.d/ipbus-sw.repo; \

        echo \
$'[amc13-uhal24-base]\n\
name     = AMC13 Software Repository (uhal version 2.4)\n\
baseurl  = http://www.cern.ch/cactus/release/amc13/1.1/slc6_x86_64/base/RPMS\n\
enabled  = 0\n\
gpgcheck = 0\n\
\n\
[amc13-uhal24-updates]\n\
name     = AMC13 Software Repository (uhal version 2.4) updates\n\
baseurl  = http://www.cern.ch/cactus/release/amc13/1.1/slc6_x86_64/updates/RPMS\n\
enabled  = 0\n\
gpgcheck = 0\n\
\n\
[amc13-uhal25-base]\n\
name     = AMC13 Software Repository (uhal version 2.5)\n\
baseurl  = http://www.cern.ch/cactus/release/amc13/1.2/slc6_x86_64/base/RPMS\n\
enabled  = 0\n\
gpgcheck = 0\n\
\n\
[amc13-uhal25-updates]\n\
name     = AMC13 Software Repository (uhal version 2.5) updates\n\
baseurl  = http://www.cern.ch/cactus/release/amc13/1.2/slc6_x86_64/updates/RPMS\n\
enabled  = 0\n\
gpgcheck = 0\n\
\n\
[amc13-uhal26-base]\n\
name     = AMC13 Software Repository (uhal version 2.6)\n\
baseurl  = http://www.cern.ch/cactus/release/amc13/1.2/slc6_x86_64/base/RPMS\n\
enabled  = 1\n\
gpgcheck = 0\n\
\n\
[amc13-uhal26-updates]\n\
name     = AMC13 Software Repository (uhal version 2.6) updates\n\
baseurl  = http://www.cern.ch/cactus/release/amc13/1.2/slc6_x86_64/updates/RPMS\n\
enabled  = 1\n\
gpgcheck = 0\n'\
        >/etc/yum.repos.d/amc13.repo; \

        echo \
$'[gemos-extras]\n\
name     = CMS GEM Online Software Repository Extras\n\
baseurl  = http://www.cern.ch/cmsgemdaq/sw/repos/slc6_x86_64/extras/RPMS\n\
enabled  = 1\n\
gpgcheck = 0\n'\
>/etc/yum.repos.d/gemos.repo; \
    elif [ "$BUILD_ARCH" = "cc7" ]; \
    then \
        echo "Building cc7 cmsgemos Docker image"; \
        if [ "$XDAQ_VER" = "14.6" ]; \
        then
            echo \
$'[xdaq-core]\n\
name     = XDAQ 14.6 Core Software\n\
baseurl  = http://xdaq.web.cern.ch/xdaq/repo/development/core/14/cc7/x86_64/RPMS/\n\
enabled  = 1\n\
gpgcheck = 0\n\
\n\
[xdaq-worksuite]\n\
name     = XDAQ 14.6 Worksuite Software\n\
baseurl  = http://xdaq.web.cern.ch/xdaq/repo/development/worksuite/14/cc7/x86_64/RPMS/\n\
enabled  = 1\n\
gpgcheck = 0\n\
\n\
[xdaq-xaas]\n\
name     = XDAQ 14.6 XaaS Software\n\
baseurl  = http://xdaq.web.cern.ch/xdaq/repo/development/xaas/14/cc7/x86_64/RPMS/\n\
enabled  = 1\n\
gpgcheck = 0\n'\
            >/etc/yum.repos.d/xdaq.repo; \
        elif [ "$XDAQ_VER" = "15" ]; \
        then
            echo \
$'[xdaq-core]\n\
name     = XDAQ 15 Core Software\n\
baseurl  = http://xdaq.web.cern.ch/xdaq/repo/development/core/r15_alpha1/cc7/x86_64/RPMS/\n\
enabled  = 1\n\
gpgcheck = 0\n\
\n\
[xdaq-worksuite]\n\
name     = XDAQ 15 Worksuite Software\n\
baseurl  = http://xdaq.web.cern.ch/xdaq/repo/development/worksuite/r15_alpha1/cc7/x86_64/RPMS/\n\
enabled  = 1\n\
gpgcheck = 0\n\
\n\
[xdaq-xaas]\n\
name     = XDAQ 15 XaaS Software\n\
baseurl  = http://xdaq.web.cern.ch/xdaq/repo/development/xaas/r15_alpha1/cc7/x86_64/RPMS/\n\
enabled  = 1\n\
gpgcheck = 0\n'\
            >/etc/yum.repos.d/xdaq.repo; \
        else
            echo \
$'[xdaq-base]\n\
name     = XDAQ Software Base\n\
baseurl  = http://xdaq.web.cern.ch/xdaq/repo/14/cc7/x86_64/base/RPMS/\n\
enabled  = 1\n\
gpgcheck = 0\n\
\n\
[xdaq-updates]\n\
name     = XDAQ Software Updates\n\
baseurl  = http://xdaq.web.cern.ch/xdaq/repo/14/cc7/x86_64/updates/RPMS/\n\
enabled  = 1\n\
gpgcheck = 0\n\
\n\
[xdaq-extras]\n\
name     = XDAQ Software Extras\n\
baseurl  = http://xdaq.web.cern.ch/xdaq/repo/14/cc7/x86_64/extras/RPMS/\n\
enabled  = 1\n\
gpgcheck = 0\n\
\n\
[xdaq-kernel-modules]\n\
name     = XDAQ Kernel Modules\n\
baseurl  = http://xdaq.web.cern.ch/xdaq/repo/14/cc7/x86_64/kernel_modules/RPMS/\n\
enabled  = 1\n\
gpgcheck = 0\n'\
            >/etc/yum.repos.d/xdaq.repo; \
        fi \

        echo \
$'[ipbus-sw-base]\n\
name     = IPBus Software Repository (version 2.5)\n\
baseurl  = http://www.cern.ch/ipbus/sw/release/2.5/centos7_x86_64/base/RPMS\n\
enabled  = 0\n\
gpgcheck = 0\n\
\n\
[ipbus-sw-updates]\n\
name     = IPBus Software Repository (version 2.5) updates\n\
baseurl  = http://www.cern.ch/ipbus/sw/release/2.5/centos7_x86_64/updates/RPMS\n\
enabled  = 0\n\
gpgcheck = 0\n\
\n\
[ipbus-sw-uhal26-base]\n\
name     = IPBus Software Repository (version 2.6)\n\
baseurl  = http://www.cern.ch/ipbus/sw/release/2.6/repos/centos7_x86_64/base/RPMS\n\
enabled  = 1\n\
gpgcheck = 0\n\
\n\
[ipbus-sw-uhal26-updates]\n\
name     = IPBus Software Repository (version 2.6) updates\n\
baseurl  = http://www.cern.ch/ipbus/sw/release/2.6/repos/centos7_x86_64/updates/RPMS\n\
enabled  = 1\n\
gpgcheck = 0\n\
\n\
[ipbus-sw-uhal26-gcc7-base]\n\
name     = IPBus Software Repository (version 2.6, gcc7)\n\
baseurl  = http://www.cern.ch/ipbus/sw/release/2.6/repos/centos7_x86_64_gcc7/base/RPMS\n\
enabled  = 0\n\
gpgcheck = 0\n\
\n\
[ipbus-sw-uhal26-gcc7-updates]\n\
name     = IPBus Software Repository (version 2.6, gcc7) updates\n\
baseurl  = http://www.cern.ch/ipbus/sw/release/2.6/repos/centos7_x86_64_gcc7/updates/RPMS\n\
enabled  = 0\n\
gpgcheck = 0\n'\
>/etc/yum.repos.d/ipbus-sw.repo; \

        echo \
$'[amc13-uhal25-base]\n\
name     = AMC13 Software Repository (uhal version 2.5)\n\
baseurl  = http://www.cern.ch/cactus/release/amc13/1.2/centos7_x86_64/base/RPMS\n\
enabled  = 0\n\
gpgcheck = 0\n\
\n\
[amc13-uhal25-updates]\n\
name     = AMC13 Software Repository (uhal version 2.5) updates\n\
baseurl  = http://www.cern.ch/cactus/release/amc13/1.2/centos7_x86_64/updates/RPMS\n\
enabled  = 0\n\
gpgcheck = 0\n\
\n\
[amc13-uhal26-base]\n\
name     = AMC13 Software Repository (uhal version 2.6)\n\
baseurl  = http://www.cern.ch/cactus/release/amc13/1.2/uhal2.6/centos7_x86_64/base/RPMS\n\
enabled  = 1\n\
gpgcheck = 0\n\
\n\
[amc13-uhal26-updates]\n\
name     = AMC13 Software Repository (uhal version 2.6) updates\n\
baseurl  = http://www.cern.ch/cactus/release/amc13/1.2/uhal2.6/centos7_x86_64/updates/RPMS\n\
enabled  = 1\n\
gpgcheck = 0\n\
\n\
[amc13-uhal26-gcc7-base]\n\
name     = AMC13 Software Repository (uhal version 2.6, gcc7)\n\
baseurl  = http://www.cern.ch/cactus/release/amc13/1.2/uhal2.6/centos7_x86_64_gcc7/base/RPMS\n\
enabled  = 0\n\
gpgcheck = 0\n\
\n\
[amc13-uhal26-gcc7-updates]\n\
name     = AMC13 Software Repository (uhal version 2.6, gcc7) updates\n\
baseurl  = http://www.cern.ch/cactus/release/amc13/1.2/uhal2.6/centos7_x86_64_gcc7/updates/RPMS\n\
enabled  = 0\n\
gpgcheck = 0\n'\
>/etc/yum.repos.d/amc13.repo; \

        echo \
$'[gemos-extras]\n\
name     = CMS GEM Online Software Repository Extras\n\
baseurl  = http://www.cern.ch/cmsgemdaq/sw/repos/centos7_x86_64/extras/RPMS\n\
enabled  = 1\n\
gpgcheck = 0\n'\
>/etc/yum.repos.d/gemos.repo; \
    fi; \

    ## Install the necessary packages

    cmd="yum -y install"; \
    if [ "$BUILD_ARCH" = "slc6" ]; \
    then \
        cmd="${cmd} sl-release sl-release-scl sl-release-scl-rh"; \
    elif [ "$BUILD_ARCH" = "cc7" ]; \
    then \
        cmd="${cmd} centos-release centos-release-scl centos-release-scl-rh"; \
    fi; \

    # epel-release conflicts with sl-release
    # cmd="${cmd} epel-release\*"; \
    cmd="${cmd} elrepo-release\*"; \
    cmd="${cmd} python-devel"; \
    echo ${cmd} && eval ${cmd}; \
    cmd="yum update -y"; \
    echo ${cmd} && eval ${cmd}; \

    ## Install xDAQ and Cactus (uHAL and AMC13)
    cmd="yum -y remove *openslp* &&"; \
    cmd="${cmd} yum groupinstall -y uhal amc13"; \

    if [[ "${XDAQVER}" =~ '^(14\.6|15)$' ]] \
    then \
        cmd="${cmd} cmsos-core \
cmsos-worksuite \
cmsos-dcs-worksuite; \
    else \
        cmd="${cmd} coretools \
database_worksuite \
extern_coretools \
general_worksuite \
hardware_worksuite \
powerpack \
extern_powerpack; \
    ## broken currently
    # cmd="${cmd} dcs_worksuite"; \
    cmd="${cmd} --exclude=daq-psx --exclude=daq-psx-devel"; \
    fi \

    echo ${cmd} && eval ${cmd}; \

    ## Install development libs and collections
    cmd="yum -y install"; \
    cmd="${cmd} gcc gcc-c++ cppcheck \
clang clang-devel clang*analyzer \
llvm llvm-devel llvm-static \
make valgrind oprofile \
mysql-devel mariadb-devel \
ncurses-devel numactl-devel readline-devel \
boost-devel libuuid-devel libusb-devel libusbx-devel \
protobuf-devel protobuf-lite-devel pugixml-devel"; \

    ## Install more extra packages
    echo "Installing extra packages for testing"; \
    cmd="${cmd} acl arp-scan wget man sudo tree htop \
numpy python-pip e2fsprogs-devel \
mysql-server mariadb-server \
graphviz libpng-devel libxml++-devel libconfuse-devel \
freeipmi-devel xinetd dnsmasq \
wiscrpcsvc wiscrpcsvc-devel \
reedmuller reedmuller-devel \
gem-peta-stage-ctp7 \
lmdb-devel \
nfs-utils nfs4-acl-tools \
curl-devel rh-git29* rh-git218*"; \

    cmd="${cmd} --exclude=\*rh-\*-scldevel\* --exclude=\*rh-\*-build\*"; \
    echo ${cmd} && eval ${cmd}; \

    echo "Installing git-lfs"; \
    # curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.rpm.sh|bash; \
    # yum -y --skip-broken install git-lfs; \

    # create the daqbuild user
    # mkdir --context=system_u:object_r:user_home_dir_t:s0 /home/daqbuild; \
    useradd -u 2055 -d /home/daqbuild daqbuild; \

    # Create a .bashrc/.profile file to set up the environment in the image
    mkdir -p /home/daqbuild/etc/profile.d; \
    echo \
$'if [ "$LD_LIBRARY_PATH" != "0" ]; then\n\
    export LD_LIBRARY_PATH\n\
    fi\n\
\n\
if [ "$XDAQ_ROOT" != "0" ]; then\n\
    export XDAQ_ROOT=/opt/xdaq\n\
    export XDAQ_DOCUMENT_ROOT=$XDAQ_ROOT/htdocs\n\
    export uHALROOT=/opt/cactus\n\
fi\n\
\n\
if [ "$LD_LIBRARY_PATH" != "0" ]; then\n\
    if [ "$LD_LIBRARY_PATH" != "" ]; then\n\
        export LD_LIBRARY_PATH=${XDAQ_ROOT}/lib:${LD_LIBRARY_PATH}\n\
        export LD_LIBRARY_PATH=${uHALROOT}/lib:${LD_LIBRARY_PATH}\n\
    else\n\
        export LD_LIBRARY_PATH=/usr/lib\n\
        export LD_LIBRARY_PATH=/usr/local/lib:${LD_LIBRARY_PATH}\n\
        export LD_LIBRARY_PATH=${XDAQ_ROOT}/lib:${LD_LIBRARY_PATH}\n\
        export LD_LIBRARY_PATH=${uHALROOT}/lib:${LD_LIBRARY_PATH}\n\
    fi\n\
else\n\
    export LD_LIBRARY_PATH=/usr/lib\n\
    export LD_LIBRARY_PATH=/usr/local/lib:${LD_LIBRARY_PATH}\n\
    export LD_LIBRARY_PATH=${XDAQ_ROOT}/lib:${LD_LIBRARY_PATH}\n\
    export LD_LIBRARY_PATH=${uHALROOT}/lib:${LD_LIBRARY_PATH}\n\
fi\n\
\n\
if [[ -n "$XDAQ_OS" ]]; then\n\
    :\n\
else\n\
    if [[ $(uname -s) = "Linux" ]]; then\n\
        XDAQ_OS=linux\n\
    elif [[ $(uname -s) = "Darwin" ]]; then\n\
        XDAQ_OS=macosx\n\
    fi\n\
    export XDAQ_OS\n\
fi\n\
\n\
## The platform is not set. Let us guess it\n\
if [[ -n "$XDAQ_PLATFORM" ]]; then\n\
    :\n\
else\n\
    if [[ $(uname -m) = "i386" ]]; then\n\
        XDAQ_PLATFORM=x86\n\
    elif [[ $(uname -m) = "i486" ]]; then\n\
        XDAQ_PLATFORM=x86\n\
    elif [[ $(uname -m) = "i586" ]]; then\n\
        XDAQ_PLATFORM=x86\n\
    elif [[ $(uname -m) = "i686" ]]; then\n\
        XDAQ_PLATFORM=x86\n\
    elif [[ $(uname -m) = "x86_64" ]]; then\n\
        XDAQ_PLATFORM=x86_64\n\
    elif [[ $(uname -m) = "Power" ]]; then\n\
        XDAQ_PLATFORM=ppc\n\
    elif [[ $(uname -m) = "Macintosh" ]]; then\n\
        XDAQ_PLATFORM=ppc\n\
    fi\n\
    XDAQ_PLATFORM=${XDAQ_PLATFORM}_$(source $XDAQ_ROOT/config/checkos.sh)\n\
    export XDAQ_PLATFORM\n\
fi\n\
\n\
export PATH=$PATH:${XDAQ_ROOT}/bin\n\
export PATH=$PATH:${uHALROOT}/bin\n\
export PATH=$PATH:${uHALROOT}/bin/amc13\n\
export AMC13_ADDRESS_TABLE_PATH=${uHALROOT}/etc/amc13/\n\
export GEMHOST=`hostname --short`\n\
export GEM_OS_PROJECT=cmsgemos\n'\
    >/home/daqbuild/etc/profile.d/gemenv.sh; \

    echo \
$'# .bash_profile\n\
\n\
# Get the aliases and functions\n\
if [ -f ~/.bashrc ]; then\n\
    . ~/.bashrc\n\
fi\n\
\n\
# User specific environment and startup programs\n\
if [ -f ~/etc/profile.d/gemenv.sh ]; then\n\
    . ~/etc/profile.d/gemenv.sh\n\
fi\n\
\n\
PATH=$PATH:$HOME/.local/bin:$HOME/bin\n\
\n\
export PATH\n'\
    >/home/daqbuild/.bash_profile; \

    if [ -d /etc/sudoers.d ]; \
    then \
        echo "daqbuild ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/daqbuild && \
        chmod 0440 /etc/sudoers.d/daqbuild; \
    fi; \
    chmod g+s -R /home/daqbuild; \
    setfacl -Rm  u::rwX,g::rwX,o::rX /home/daqbuild; \
    setfacl -Rdm u::rwX,g::rwX,o::rX /home/daqbuild; \
    ls -lZ /home; \
    # Create mount point for attached volumes
    mkdir /data; \
    chmod g+s -R /data; \
    chown daqbuild:daqbuild -R /data; \
    setfacl -Rm  u::rwX,g::rX,o::rX /data; \
    setfacl -Rdm u::rwX,g::rX,o::rX /data; \
    cat /etc/passwd; \
    cat /etc/group; \

    echo \
$'source /opt/rh/rh-git218/enable\n' \
    > /etc/profile.d/git.sh; \
elif [ "${EXTEND}" = "1" ]; \
then \
    cmd="yum -y install"; \
    if [ "${EXTRARUBY}" = "1" ]; \
    then \
        echo "Installing different ruby versions"; \
        rubyvers=( rh-ruby22 rh-ruby23 rh-ruby24 rh-ruby25 ); \
        for rver in "${rubyvers[@]}"; \
        do \
            cmd="${cmd} ${rver} ${rver}-rubygems ${rver}-\*"; \
        done; \
        cmd="${cmd} \*rubygem-simplecov"; \
        cmd="${cmd} --exclude=rh-ruby\*-build --exclude=rh-ruby\*-scldevel"; \
    fi; \

    if [ "${DEVTOOLS}" = "1" ]; \
    then \
        echo "Installing clang/llvm"; \
        cmd="yum -y install \
clang clang-analyzer clang-devel \
llvm3.9 llvm3.9-devel \
llvm5.0 llvm5.0-devel \
llvm7.0 llvm7.0-devel \
llvm\*-static \
devtoolset-7-llvm devtoolset-7-llvm-devel \
llvm-toolset-7-\* \
devtoolset-8-llvm devtoolset-8-llvm-devel \
llvm-toolset-8-\*"; \

        echo "Installing devtoolsets 3, 4, 6, 7, 8"; \
        devtools=( devtoolset-3 devtoolset-4 devtoolset-6 devtoolset-7 devtoolset-8 ); \
        for dtool in "${devtools[@]}"; \
        do \
            # cmd="${cmd} ${dtool}"; \
            # tools=( binutils build elfutils gcc gdb lib make oprofile toolchain valgrind ); \
            ### perftools: oprofile systemtap valgrind dyninst
            ### build: maven java
            ### toolchain: binutils dwz elfutils gcc gcc-c++ gcc-gfortran gdb ltrace make memstomp strace
            tools=( perftools toolchain libgccjit libgccjit-devel libstdc++-devel ); \
            for tool in "${tools[@]}"; \
            do \
                cmd="${cmd} ${dtool}-${tool}"; \
            done; \
        done; \
    fi; \

    if [ "${EXTRAPYTHON}" = "1" ]; \
    then \
        # Extra python versions, should be in the path?
        echo "Adding pypy"; \
        cmd="${cmd}  *pypy*"; \
        # Except for python27 on slc6, which requires an scl enable equivalent
        echo "Installing extra rh/scl python versions"; \
        sclpyvers=( python27 python33 python34 python36 ); \
        pypkgs=( build coverage devel docutils \
importlib pip setuptools virtualenv \
MySQL numpy scipy simplejson \
python-devel \
six sphinx test wheel ); \

        for pypkg in "${pypkgs[@]}"; \
        do \
            cmd="${cmd} python-${pypkg}"; \
        done; \

        for sclpy in "${sclpyvers[@]}"; \
        do \
            cmd="${cmd} ${sclpy}"; \
            for pypkg in "${pypkgs[@]}"; \
            do \
                cmd="${cmd} ${sclpy}-${pypkg}"; \
            done; \
        done; \

        rhpyvers=( rh-python34 rh-python35 rh-python36 ); \
        for rhpy in "${rhpyvers[@]}"; \
        do \
            cmd="${cmd} ${rhpy}*"; \
            for pypkg in "${pypkgs[@]}"; \
            do \
                cmd="${cmd} ${rhpy}-\*${pypkg}"; \
            done; \
        done; \
    fi; \

    # if [ "${EXTRAROOT}" = "1" ]; \
    # then \
    #     cmd="${cmd}  root root-\* python\*root"; \
    # fi; \

    cmd="${cmd} --exclude=*scldevel*"; \
    echo ${cmd} && eval ${cmd}; \
fi; \
cmd="yum clean all"; \
echo ${cmd} && eval ${cmd}; \
cmd="rm -rf /var/cache/yum"; \
echo ${cmd} && eval ${cmd};

RUN \
if [ "${EXTEND}" = "1" ] && [ "${EXTRAROOT}" = "1" ]; \
then \
    gccvers=( 4.4 4.7 4.8 4.9 5.1 ); \
    rootvers=( v5.34.28 v5.34.36 v6.10.08 v6.12.06 v6.13.08 v6.14.06 v6.16.00 ); \

    echo "Installing ROOT"; \
    cmd="yum -y install libXpm"; \
    echo ${cmd} && eval ${cmd};

    for rootver in "${rootvers[@]}"; \
    do \
        for gccver in "${gccvers[@]}"; \
        do \
            if [[ "${BUILD_ARCH}" =~ cc7 ]]; \
            then \
                if  [[ "${gccver}" =~ 4.9 ]] && ! [[ "${rootver}" =~ ^v5.3 ]]; \
                then \
                    echo "Skipping root_${rootver}-gcc${gccver} for OS version ${BUILD_ARCH}"; \
                    continue; \
                elif ! [[ "${gccver}" =~ 4.[89] ]]; \
                then \
                    echo "Skipping root_${rootver}-gcc${gccver} for OS version ${BUILD_ARCH}"; \
                    continue; \
                fi; \
            elif [[ "${BUILD_ARCH}" =~ slc6 ]]; \
            then \
                if ! [[ "${rootver}" =~ ^v5.3 ]]; \
                then \
                    echo "Skipping root_${rootver}-gcc${gccver} for OS version ${BUILD_ARCH}"; \
                    continue; \
                elif [[ "${rootver}" =~ ^v5.34.28 ]] && [[ "${gccver}" =~ 5.1 ]]; \
                then \
                    echo "Skipping root_${rootver}-gcc${gccver} for OS version ${BUILD_ARCH}"; \
                    continue; \
                fi; \
            fi; \
            mkdir -p /opt/root/${rootver}-gcc${gccver} && \
            cd /opt/root/${rootver}-gcc${gccver}; \

            if [ "${BUILD_ARCH}" = "slc6" ]; \
            then \
                wget -nv https://root.cern.ch/download/root_${rootver}.Linux-slc6-x86_64-gcc${gccver}.tar.gz; \
            elif [ "${BUILD_ARCH}" = "cc7" ]; \
            then \
                if [[ "${rootver}" =~ v5.34.28 ]]; \
                then \
                    wget -nv https://root.cern.ch/download/root_${rootver}.Linux-cc7-x86_64-gcc${gccver}.tar.gz; \
                else \
                    wget -nv https://root.cern.ch/download/root_${rootver}.Linux-centos7-x86_64-gcc${gccver}.tar.gz; \
                fi; \
            fi; \

            if [ -f root_${rootver}*gcc${gccver}.tar.gz ]; \
            then \
                tar xzf root_${rootver}*gcc${gccver}.tar.gz && \
                rm -rf root_${rootver}*gcc${gccver}.tar.gz; \
            fi; \
        done; \
    done; \

    unset gccvers; \
    unset rootvers; \

elif [ "${EXTEND}" = "1" ] && [ "${EXTRAPYTHON}" = "1" ]; \
then \
     echo pip install -U setuptools pip; \

elif [ "${EXTEND}" = "1" ] && [ "${EXTRARUBY}" = "1" ]; \
then \
    # Install some GEMs for common tasks
    echo "Setting up ruby 2.4"; \
    sudo -u daqbuild -i &&  \
    source /opt/rh/rh-ruby24/enable && \
    gem install \
        bundler \
        ghi \
        github_changelog_generator \
        katip; \
    # scl enable rh-ruby24; \
    echo "Setting up git 2.18"; \
    . /opt/rh/rh-git218/enable; \
fi; \
cmd="yum clean all"; \
echo ${cmd} && eval ${cmd}; \
cmd="rm -rf /var/cache/yum"; \
echo ${cmd} && eval ${cmd};

# RUN echo "Checking availability of daqbuild account/group:"; \
#   getent passwd|fgrep daq; \
#   getent group|fgrep daq; \
#   sudo -u daqbuild whoami; \
#   sudo -u daqbuild sudo -ll; \

RUN yum update -y && yum clean all && rm -rf /var/cache/yum
    
VOLUME /data /data/bigdisk/sw/peta-stage /data/bigdisk/sw/Xilinx/SDK /home/daqbuild

# Only want to set these in the built image, but need to build it first...
# USER daqbuild:daqbuild
ENV HOME /home/daqbuild
ENV BUILD_HOME /home/daqbuild
ENV DATA_PATH /data/
ENV ELOG_PATH /home/daqbuild/elog
ENV PETA_STAGE /data/bigdisk/sw/peta-stage
ENV XILINX_SDK /data/bigdisk/sw/Xilinx/SDK
WORKDIR $HOME
CMD /bin/bash
